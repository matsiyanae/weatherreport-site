/*
    Get user input for 2 cities/locations
    Validate is for presence
    Post to API
    Show results
*/

import React from "react";
import Search from "./components/Search";

export default class HomeView extends React.Component {
    constructor (props) {
        super(props);
        this.state = {
            locationone : '',
            locationtwo : '',
            showTip: false,
            showResults: <div></div>,
            tipMessage:'Both fields are required.'
        }
        this.setLocationOne = this.setLocationOne.bind(this);
        this.setLocationTwo = this.setLocationTwo.bind(this);
        this.postLocations = this.postLocations.bind(this);
    }
    
    setLocationOne(input) {
        this.setState({
            locationone : input
        }, ()=>{
            this.validateFields();
        })
    }
        
    setLocationTwo(input) {
        this.setState({
            locationtwo : input
        }, ()=>{
            this.validateFields();            
        })
    }
    
    validateFields(){
        let showTip = this.state.showTip;
        let showResults = this.state.showResults;
        if(this.state.locationone === this.state.locationtwo){
            this.setState({
                showTip: true,
                tipMessage: 'Cities can not be the same'            
            });
            return
        }

        if(this.state.locationone.length > 3 && this.state.locationtwo.length > 3){
            showTip = false;
            this.postLocations();
        } else {
            showTip = true;            
        }

        this.setState({
            showTip: showTip                
        });

    }

    postLocations(){
        try {
            let self = this;
            self.setState({
                    showResults: <p className={'col-sm-12 text-center'}>{'please wait...'}</p>
                },()=>{
                    //TODO
                    //URL BAse should be stored in Global variable
                    fetch('http://localhost:52676/api/weatherreport', {
                        method: 'POST',
                        headers: {
                            'Content-Type': 'application/json'
                        },
                        body: JSON.stringify({
                            cityone: this.state.locationone,
                            citytwo: this.state.locationtwo,
                        })
                    }).then(function(response) {
                        return response.json()              
                    }).then(function(json) {
                        //We prefer the json result
                        if(json.Status === 200){
                            let cityOne = JSON.parse(JSON.parse(json.Data).cityone);
                            let cityTwo = JSON.parse(JSON.parse(json.Data).citytwo);
                            /*          
                            Result object structure      
                            {
                                place = cityOneData.name,
                                currentTemp = cityOneData.main.temp,
                                maxTemp = cityOneData.main.temp_max,
                                minTemp = cityOneData.main.temp_min,
                                description = descriptionOne,
                                humidity = cityOneData.main.humidity,
                                pressure = cityOneData.main.pressure,
                                windSpeed = cityOneData.wind.speed,
                                windDirection = cityOneData.wind.deg,
                            }
                            */

                            let tempDiff = cityTwo.currentTemp-cityOne.currentTemp;
                            let humidityDiff = cityOne.humidity-cityTwo.humidity;

                            //Messy language construction
                            let results = <div className={'col-md-12'}>
                             <div  className={'col-sm-6 text-left'}>
                                <div className={''}>{'Curently we are looking at '+cityOne.description+' for '+cityOne.place + ' and '+cityTwo.description+' for '+cityTwo.place+'.'}</div>                        
                                <div className={''}>{cityOne.place+' is '+(cityTwo.currentTemp>cityOne.currentTemp ? (tempDiff)+' degrees cooler': (tempDiff*-1)+' degrees warmer')+ ' at '+cityOne.currentTemp+' Degrees Celcius.'}</div>
                                <div className={''}>{'While '+cityTwo.place+' is '+(cityOne.humidity>cityTwo.humidity ? (humidityDiff)+' percent less humid': (humidityDiff*-1)+' percent more humid')+ ' at '+cityTwo.humidity+ '%.'}</div>
                                <div className={''}>{'So if you are in '+cityOne.place+' you might decide to leave home '+(cityOne.humidity>cityTwo.humidity ? 'with ': '')+ 'your umbrella compared to your counterparts in '+cityTwo.place+'.'}</div>                        
                                <div className={''}>{'Wind speed is at '+cityOne.windSpeed+' knots for '+cityOne.place + ' and '+cityTwo.windSpeed+' knots for '+cityTwo.place+'.'}</div>                        
                                <div className={''}>{'Bye bye :)'}</div>                        
                             </div>
                            </div>;
                            self.setState({
                                showResults: results
                            });
                        } else {
                            self.setState({
                                showResults: <p className={'col-sm-12 text-center'}>{json.Message?json.Message:"Please try again"}</p>
                            });
                        }

                    }).catch(function(ex) {
                        //handle any errors heree
                        self.setState({
                            showResults: <p className={'col-sm-12 text-center'}>{'please try retype the cities again...'}</p>
                        });
                    })
            });

        } catch (error) {
            //worst case scenario, somethine unprecedented has occured. handle it here    
            this.setState({
                showResults: <p className={'col-sm-12 text-center'}>{'please try retype the cities again...'}</p>
            });        
        }

    }

    render() {
        return  <div className={'col-sm-12'} >
            <h3 className={'text-center'}>{'Enter two city names below to compare their current weather'}</h3>            
            <form  className={'col-sm-12'}>
                <Search value={this.state.locationone} showTip={this.state.showTip} tipMessage={this.state.tipMessage} setLocationName={this.setLocationOne} placeholder={'Enter location one'}/>
                <Search value={this.state.locationone} setLocationName={this.setLocationTwo} placeholder={'Enter location two'}/>
            </form>      
            {this.state.showResults}      
        </div>
    }
}