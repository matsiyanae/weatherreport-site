/*
Take user input and use Google maps Autocomplete to get bets match 
*/

import React from "react";
import {FormGroup, ControlLabel,FormControl,HelpBlock,MenuItem } from 'react-bootstrap';
export default class Search extends React.Component {
    static get defaultProps() {
        return {
        };
    }

    constructor (props) {
        super(props);
        this.state = {
            value : this.props.value,
            tip: <div></div>,
            predictions:[],
            showList: 'hidden'
        }
        this.handleChange = this.handleChange.bind(this);
        this.service = new google.maps.places.AutocompleteService();
    }
    
    componentDidMount() {
        if (this.props.value) {
            this.placeService = new google.maps.places.PlacesService(this.refs.div);
            this.placeService.getDetails({placeId: this.props.value}, (e, status) => {
                if(status === 'OK') {
                    this.setState({ value: e.formatted_address });
                    //this.refs.input.value = e.formatted_address ;
                }
            });
        }
    }

    handleChange(e) { //Handle user input, Show autocomplete results and assign to state list
        this.setState({
            value:e.target.value
        });
        const { types=['(cities)'] } = this.props;
        if(e.target.value) {
            this.service.getPlacePredictions({input: e.target.value, types}, (predictions, status) => {
                if (status === 'OK' && predictions && predictions.length > 0) {
                    this.setState({
                        predictions:predictions,
                        showList: 'dropdown-menu open'
                    });

                } else {
                    this.setState({
                        predictions:[],
                        showList: 'hidden'
                    });
                }
            });
        } else {
            //this.props.onClose();
            this.setState({
                predictions:[],
                showList: 'hidden'
            });
        }
    }

    render() {
        let self = this;
        return (
            <FormGroup
                className={'col-sm-6'}
                controlId="formBasicText">
                <FormControl
                    type="text"
                    placeholder={this.props.placeholder}
                    value={this.state.value}
                    onChange={this.handleChange}/>
                <div ref="div"></div>
                    <ul className={this.state.showList}>
                        {this.state.predictions && this.state.predictions.map((item, index) =>
                            <MenuItem key={index} eventKey={1} onSelect={()=>{
                                this.setState({
                                    predictions:[],
                                    showList: 'hidden',
                                    value: item.description
                                });
                                self.props.setLocationName(item.description);
                            }}>
                            {item.description}
                            </MenuItem>
                        )}
                    </ul>
                {this.state.tip}
            </FormGroup>
    );
    }
}