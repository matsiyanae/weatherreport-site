//Include required modules
var gulp = require("gulp"),
    babelify = require('babelify'),
    browserify = require("browserify"),
    connect = require("gulp-connect"),
    source = require("vinyl-source-stream")
;


//Copy html to build folder
gulp.task("copyHtml", function(){
    return gulp.src("./src/html/*.*")
    .pipe(gulp.dest("./build"));
});

//Copy css to build folder
gulp.task("copyCSS", function(){
    return gulp.src("./src/styles/*.css")
    .pipe(gulp.dest("./build/styles"));
});

//TODO
//1. Add Watch for Dev 
//2. Minify all resources


//Convert ES6 code in all js files in src/js folder and copy to 
//build folder as bundle.js
gulp.task("build", function(){
    return browserify({
        entries: ["./src/js/index.js"],
        extensions: [".js", ".jsx"]
    })
    .transform(babelify.configure({
        presets : ["es2015", "react"]
    }))
    .bundle()
    .pipe(source("bundle.js"))
    .pipe(gulp.dest("./build"))
    ;
});

//Start a test server with doc root at build folder and 
//listening to 9001 port. Home page = http://localhost:9001
gulp.task("startServer", function(){
    connect.server({
        root : "./build",
        livereload : true,
        port : 9001
    });
});

//Default task. This will be run when no task is passed in arguments to gulp
gulp.task("default",["copyHtml", "copyCSS", "build", "startServer"]);